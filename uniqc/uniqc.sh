# spawn parallel "uniq -c" jobs on *unsorted* lists
jobs="1 2"
for j in $jobs; do
	cat keys_${j}.txt | ./uniqc > keys_${j}_c.txt &
done

# wait for parallel jobs
wait

# combine the results
cat keys_*_c.txt | ./uniqc join > keys_c.txt

# the word "join" after ./uniqc is an example
# any word will work since argc > 1 is needed
