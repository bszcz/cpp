// Copyright (c) 2013 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

#include <iostream>
#include <map>
#include <string>

using namespace std;

int main(int argc, char* argv[]) {
	map<string, long> dict;
	long val = 1;
	string key;
	while (argc > 1 ? cin >> val >> key : cin >> key) {
		if (dict.find(key) == dict.end()) {
			dict[key]  = val;
		} else {
			dict[key] += val;
		}
	}

	map<string, long>::iterator dict_it;
	for (dict_it = dict.begin(); dict_it != dict.end(); dict_it++) {
		cout << (*dict_it).second << " " << (*dict_it).first << endl;
	}

	return 0;
}
