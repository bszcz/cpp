// Copyright (c) 2007 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

/*****************************************************************
 *
 *  name:     Bartosz Szczesny
 *  username: mbcx6bs2
 *  date:     19.10.2007
 *
 *****************************************************************/

#include <iostream>
#include <cmath>

using namespace std;

double van_der_waals(double vdw_a, double vdw_b, double T, double V);

int main(void) {
	cout << "Please enter the Van der Waals parameters for the substance you are using:" << endl;
	double vdw_a = 0.0;
	cout << "What is a, the attractive term (Jm^3/mol)?" << endl;
	cin >> vdw_a;
	double vdw_b = 0.0;
	cout << "What is b, the excluded volume (m^3/mol)?" << endl;
	cin >> vdw_b;

	char a;
	do {
		cout << "Do you wish to enter a temperature and volume? ([Y]es or [N]o)" << endl;
		cin >> a;

		if (a == 'y' || a == 'Y') {
			char temp_unit;
			cout << "Is the temperature in [K]elvin or [C]elsius ?" << endl;
			cin >> temp_unit;

			double temp = 0.0;
			if (temp_unit == 'k' || temp_unit == 'K') {
				cout << "Please enter the temperature (K)" << endl;
				cin >> temp;
			}
			if (temp_unit == 'c' || temp_unit == 'C') {
				cout << "Please enter the temperature (C)" << endl;
				cin >> temp;
				temp += 273.15;
			}

			double volume = 0.0;
			cout << "Please enter the volume (m^3)" << endl;
			cin >> volume;

			double pressure = van_der_waals(vdw_a, vdw_b, temp, volume);
			cout  << "One mole of gas at " << temp
			      << " K in volume " << volume
			      << " m^3 has a pressure of " << pressure
			      << " Pa." << endl;
		} // end of if loop inside do loop

	} while (a == 'y' || a == 'Y');    // end of do loop

	cout << "Thank you for using the Van der Waals gas pressure calculator." << endl;
	return 0;
} // int main(void)

double van_der_waals(double vdw_a, double vdw_b, double temp, double volume) {
	const double R = 8.315;
	double pressure = R * temp / (volume - vdw_b) - vdw_a / pow(volume, 2);
	if (pressure < 0.0) {
		cout << "The negative pressure is unphysical." << endl;
	}
	return pressure;
}
