// Copyright (c) 2007 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

/*************************************************
**   Full Name: Bartosz Szczesny
**   Username : mbcx6bs2
**   Date     : 05.10.2007
*************************************************/

#include <iostream>
using namespace std;

int main(void) {

	double mass     = 0.0;
	double speed    = 0.0;
	double k_energy = 0.0;

	cout << "Please enter the mass (kg)" << endl;
	cin >> mass;

	cout << "Please enter the speed (m/s)" << endl;
	cin >> speed;

	k_energy = 0.5 * mass * speed * speed;

	cout << "A mass of " << mass
	     << " kg at a speed of " << speed
	     << " m/s has a kinetic energy of " << k_energy
	     << " J." << endl << endl;

	cout << "Thank you for using the kinetic energy calculator." << endl;

	return 0;
}
