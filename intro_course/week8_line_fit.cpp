// Copyright (c) 2007 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

/**************************************************************************************
 *
 *  Username: mbcx6bs2
 *  Name:     Bartosz Szczesny
 *  Date:     23.11.2007
 *
 *************************************************************************************/

#include <iostream>
#include <cstring>
#include <fstream>
#include <cstdlib>
#include <cmath>

using namespace std;

// structure declaration
struct point {
	double x;
	double y;
	double s;
	double mxpc;
	double res;
};

struct sum {
	double wx;
	double wy;
	double wx2;
	double wxy;
	double w;
};

// prototype function declaration(s)

int main(void) {
	const int LENMAX = 1000;
	const int STRMAX = 50;

	char inpath[STRMAX] = {};
	cout << "Please enter the input filename: ";
	cin >> inpath;
	char outpath[STRMAX] = {};
	cout << "Please enter the output filename: ";
	cin >> outpath;

	// open infile and check if ok
	ifstream in(inpath);
	if (!in) {
		cerr << "Failed to open input file " << inpath << endl;
		exit(EXIT_FAILURE);
	}

	// read values from infile to arrays
	point dp[LENMAX];
	int len = 0;
	while (len < LENMAX && in >> dp[len].x >> dp[len].y >> dp[len].s) { // safe as && operates left-to-right
		len++;
	}
	in.close();

	// calculate the sums
	sum S = {};
	for (int i = 0; i < len; i++) {
		S.w += 1 / pow(dp[i].s, 2);
		S.wx += dp[i].x / pow(dp[i].s, 2);
		S.wy += dp[i].y / pow(dp[i].s, 2);
		S.wx2 += dp[i].x * dp[i].x / pow(dp[i].s, 2);
		S.wxy += dp[i].x * dp[i].y / pow(dp[i].s, 2);
	}

	// main calcualtions
	double D = (S.w * S.wx2 - pow(S.wx, 2));
	double m = (S.w * S.wxy - S.wx * S.wy) / D;
	double dm = sqrt(S.w / D);
	double c = (S.wy * S.wx2 - S.wx * S.wxy) / D;
	double dc = sqrt(S.wx2 / D);

	// calculate mx+c, residuals and sum of residuals squared
	double xsq = 0.0;
	for (int i = 0; i < len; i++) {
		dp[i].mxpc = m * dp[i].x + c;
		dp[i].res = (dp[i].mxpc - dp[i].y) / dp[i].s;
		xsq += pow(dp[i].mxpc - dp[i].y, 2) / pow(dp[i].s, 2);
	}

	// open/create outfile and check if ok
	ofstream out(outpath);
	if (!out) {
		cerr << "Failed to open input file " << inpath << endl;
		exit(EXIT_FAILURE);
	}

	// write header to outfile
	out << "For y = mx + c, with errors on y of sigma, the line of best fit has" << endl
	    << endl << '\t' << "m = " << m << " +/- " << dm
	    << endl << '\t' << "c = " << c << " +/- " << dc << endl
	    << endl << "The data and the fitted points are: " << endl << endl
	    << "\tx\ty\tsigma\tmx+c\tresidual" << endl << endl;

	// write values to outfile
	for (int i = 0; i < len; i++) {
		out << '\t' << dp[i].x << '\t' << dp[i].y << "\t" << dp[i].s << "\t" << dp[i].mxpc << "\t" << dp[i].res << endl;
	}

	// and finally, write the values of xsq to out file
	out << endl << "Chi-squared = " << xsq << endl;
	out.close();
	return 0;
} // int main(void)
