// Copyright (c) 2007 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

/*************************************************
**   Full Name: Bartosz Szczesny
**   Username : mbcx6bs2
**   Date     : 05.10.2007
*************************************************/

#include <iostream>
using namespace std;

int main(void) {
	const double IDEAL_GAS = 8.315;

	double temp  = 0.0;
	double vol   = 0.0;
	double press = 0.0;

	cout << "Please enter the temperature (K)" << endl;
	cin >> temp;

	cout << "Please enter the volume (m^3)" << endl;
	cin >> vol;

	press = IDEAL_GAS * temp / vol;

	cout << "One mole of gas at " << temp
	     << " K in volume " << vol
	     << " m^3 has a pressure of " << press << " Pa." << endl << endl;

	cout << "Thank you for using the ideal gas pressure calculator." << endl;

	return 0;
}
