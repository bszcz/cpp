// Copyright (c) 2007 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

/*********************************************************************************
 *
 *  Name:     Bartosz Szczesny
 *  Username: mbcx6bs2
 *  Date:     26.10.2007
 *
 *********************************************************************************/

#include <iostream>

using namespace std;

int main(void) {
	const int LENMAX = 1000;
	int arr[LENMAX] = {};
	int len = 0;
	for (int i = 0; i < LENMAX; i++) {
		cout << "Please enter an integer (0 to end): ";
		cin >> arr[i];
		if (0 == arr[i]) {
			break;
		}
		len++;
	}

	cout << endl << "The unsorted array is:" << endl;
	for (int i = 0; i < len; i++) {
		cout << '\t' << "Element " << i << ":" << '\t' << arr[i] << endl;
	}

	for (int i = (len - 1); i > 0; i--) {
		for (int j = 0; j < i; j++) {
			if (arr[j] > arr[j + 1]) {
				int temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
			}
		}
	}

	cout << endl << "The sorted array is:" << endl;
	for (int i = 0; i < len; i++) {
		cout << '\t' << "Element " << i << ":" << '\t' << arr[i] << endl;
	}

	cout << endl << "Thank you for using this array sorting software." << endl;
	return 0;
} // int main(void)
