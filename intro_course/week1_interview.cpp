// Copyright (c) 2007 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

/*************************************************
**   Full Name: Bartosz Szczesny
**   Username : mbcx6bs2
**   Date     : 28.09.2007
*************************************************/

#include <iostream>
using namespace std;

int main(void) {
	const int STRMAX = 50;
	char nickname[STRMAX];
	char colour[STRMAX];
	char day[STRMAX];
	char holiday[STRMAX];

	cout << "What's your nick-name?" << endl;
	cin >> nickname;

	cout << "What's your favourite snooker ball colour?" << endl;
	cin >> colour;

	cout << "What's your favourite day of the week?" << endl;
	cin >> day;

	cout << "What's your favourite holiday destination?" << endl;
	cin >> holiday;

	cout << "My nick-name is " << nickname << endl;
	cout << "My favourite snooker ball colour is " << colour << endl;
	cout << "My favourite day of the week is " << day << endl;
	cout << "My favourite holiday destination is " << holiday << endl;
	return 0;
}
