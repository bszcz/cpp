// Copyright (c) 2007 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

/*************************************************
**   Full Name: Bartosz Szczesny
**   Username : mbcx6bs2
**   Date     : 28.09.2007
*************************************************/

#include <iostream>
using namespace std;

int main(void) {
	cout << "My name is Bartosz Szczesny\n"
	     << "My lab day is Tuesday\n"
	     << "The school's address is:\n"
	     << "\tSchool of Physics and Astronomy\n"
	     << "\tUniversity of Manchester\n"
	     << "\tM13 9PL\n"
	     << "\tUK\n";
	return 0;
}
