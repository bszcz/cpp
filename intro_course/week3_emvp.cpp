// Copyright (c) 2007 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

/*************************************************
**   Full Name: Bartosz Szczesny
**   Username : mbcx6bs2
**   Date     : 12.10.2007
*************************************************/

#include <iostream>
using namespace std;

int main() {
	double mass = 0.0;
	cout << "Please enter the mass (kg)" << endl;
	cin >> mass;

	char answer;
	cout << "Are you going to enter a [S]peed or a [M]omentum ?" << endl;
	cin >> answer;

	if (answer == 'S' || answer == 's') {
		double speed = 0.0;
		cout << "Please enter the speed (m/s)" << endl;
		cin >> speed;

		double k_energy = 0.5 * mass * speed * speed;

		cout << "A mass of " << mass
		     << " kg at a speed of " << speed
		     << " m/s has a kinetic energy of " << k_energy
		     << " J." << endl;
	}

	if (answer == 'M' || answer == 'm') {
		double momentum = 0.0;
		cout << "Please enter the momentum (kgm/s)" << endl;
		cin >> momentum;

		double k_energy = (momentum * momentum) / (2 * mass);

		cout << "A mass of " << mass
		     << " kg with a momentum of " << momentum
		     << " kgm/s has a kinetic energy of " << k_energy
		     << " J." << endl;
	}

	if (answer != 'S' && answer != 'M' && answer != 's' && answer != 'm') {
		cout << "You entered a mass of " << mass
		     << " kg, but your choice of speed or momentum was not a valid choice." << endl;
	}

	cout << "Thank you for using the kinetic energy calculator." << endl;
	return 0;
} // int main(void)
