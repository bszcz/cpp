// Copyright (c) 2007 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

/*************************************************
**   Full Name: Bartosz Szczesny
**   Username : mbcx6bs2
**   Date     : 28.09.2007
*************************************************/

#include <iostream>
using namespace std;

int main(void) {
	cout << "Hello, World!" << endl;
	return 0;
}
