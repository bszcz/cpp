// Copyright (c) 2007 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

/***************************************************************************
 *
 * Username:  mbcx6bs2
 * Full name: Bartosz Szczesny
 * Date:      30.11.2007
 *
 **************************************************************************/

/**************************************************************************
 *
 * I learnt how the heap sort algorithm works at:
 *
 * http://cis.stvincent.edu/html/tutorials/swd/heaps/heaps.html
 *
 * and then, wrote this entire program myself.
 *
 **************************************************************************/

#include <iostream>
#include <cstring>
#include <fstream>
#include <cstdlib>
#include <cmath>
#include <ctime>

using namespace std;

const int STRMAX = 32;

struct nn {
	char first[STRMAX];
	char second[STRMAX];
	int mobile;
};

int cmp_mobile(int p, int h, nn** p_person);
int cmp_first(int p, int h, nn** p_person);
int cmp_second(int p, int h, nn** p_person);
int swap(int one, int two, nn** p_person);

int main(void) {
	char inpath[STRMAX] = {};
	cout << "Please enter the input filename: ";
	cin >> inpath;
	char outpath[STRMAX] = {};
	cout << "Please enter the output filename: ";
	cin >> outpath;

	char c;
	do {
		cout << "Do you wish to sort by [f]irst name, [s]econd name, or [m]obile number? ";
		cin >> c;
	} while (c != 'f' && c != 'F' && c != 's' && c != 'S' && c != 'm' && c != 'M');

	// open infile and check if ok
	ifstream in(inpath);
	if (!in) {
		cerr << "Failed to open input file " << inpath << endl;
		exit(EXIT_FAILURE);
	}

	// count values in infile
	char char_tmp[STRMAX] = {};
	int int_tmp;
	int len = 0;
	while (in >> char_tmp >> char_tmp >> int_tmp) {
		len++;
	}

	nn* person = new nn[len];     // allocate memory for an array of structures
	nn** p_person = new nn*[len]; // allocate memory for an array of pointers to structures
	for (int i = 0; i < len; i++) {
		p_person[i] = &(person[i]); // assign addresses of structures to poniters
	}

	in.clear();
	in.seekg(0);
	for (int i = 0; i < len; i++) {
		in >> person[i].first >> person[i].second >> person[i].mobile;
	}
	in.close();

	// open outfile and check if ok
	ofstream out(outpath);
	if (!out) {
		cerr << "Failed to open output file " << outpath << endl;
		exit(EXIT_FAILURE);
	}

	out << "The unsorted list: " << endl;
	for (int i = 0; i < len; i++) {
		out << endl << '\t' << person[i].first << "\t\t" << person[i].second << "\t\t07" << person[i].mobile;
	}

	/**************************
	 *
	 *  some important
	 *  heap algebra
	 *
	 *  parent      = p
	 *  left child  = 2*p+1
	 *  right child = 2*p+2
	 *
	 **************************/

	clock_t start = clock();

	int H = len - 1; // heap size
	int h = len - 1; // heap size index
	int P = (H - 1) / 2; // no of parents - form 0 to P
	int p = (h - 1) / 2; // parent index
	int d; // start point of the down sort

	// build the initial heap
	for (int i = P; i > -1; i--) {
		switch (c) {
		case 'm':
		case 'M':
			d = cmp_mobile(i, h, p_person);
			while (d <= p) {
				d = cmp_mobile(d, h, p_person);
			}
			break;
		case 'f':
		case 'F':
			d = cmp_first(i, h, p_person);
			while (d <= p) {
				d = cmp_first(d, h, p_person);
			}
			break;
		case 's':
		case 'S':
			d = cmp_second(i, h, p_person);
			while (d <= p) {
				d = cmp_second(d, h, p_person);
			}
			break;
		}
	} // initial heap built

	// sort the heap
	for (;;) {
		swap(0, h, p_person); // element zero is largest - put at the end of heap
		h--; // decrease the heap size
		p = (h - 1) / 2;
		if (h < 1) {
			break; // break if heap size == 0
		}

		// sort starting from the top
		switch (c) {
		case 'm':
		case 'M':
			d = cmp_mobile(0, h, p_person);
			while (d <= p) {
				d = cmp_mobile(d, h, p_person);
			}
			break;
		case 'f':
		case 'F':
			d = cmp_first(0, h, p_person);
			while (d <= p) {
				d = cmp_first(d, h, p_person);
			}
			break;
		case 's':
		case 'S':
			d = cmp_second(0, h, p_person);
			while (d <= p) {
				d = cmp_second(d, h, p_person);
			}
			break;
		}
	} // heap sorted

	clock_t finish = clock();
	double duration = (double)(finish - start) / CLOCKS_PER_SEC;
	cout << endl << "duration: " << duration << " s" << endl;

	out << endl << endl << endl << "The sorted list: ";
	switch (c) {
	case 'm':
	case 'M':
		out << endl << "(sorted by mobile phone number)" << endl;
		break;
	case 'f':
	case 'F':
		out << endl << "(sorted by first name)" << endl;
		break;
	case 's':
	case 'S':
		out << endl << "(sorted by second name)" << endl;
		break;
	}
	for (int i = 0; i < len; i++) {
		out << endl << '\t' << p_person[i]->first << "\t\t" << p_person[i]->second << "\t\t07" << p_person[i]->mobile;
	}

	in.close();
	delete [] person;
	delete [] p_person;
	return 0;
} // int main(void)

//
// function definitions
//

int swap(int one, int two, nn** p_person) {
	nn* p_person_tmp  = *(p_person + one);
	*(p_person + one) = *(p_person + two);
	*(p_person + two) = p_person_tmp;
	return two;
}

int cmp_mobile(int pp, int h, nn** p_person) {
	int parent = h;
	if ((2 * pp + 2) <= h) { // has two children?
		if ((*(p_person + 2 * pp + 2))->mobile > (*(p_person + 2 * pp + 1))->mobile // right bigger than left ?
		    && (*(p_person + 2 * pp + 2))->mobile > (*(p_person + pp))->mobile) { // AND right bigger that partent ?
			parent = swap(pp, 2 * pp + 2, p_person); // element changed, need to sort down the branch
		} else { // right NOT bigger than left and parent
			if ((*(p_person + 2 * pp + 1))->mobile > (*(p_person + pp))->mobile) { // left child bigger?
				parent = swap(pp, 2 * pp + 1, p_person); // element changed, need to sort down the branch
			}
		}
	} else { // both children compared
		if ((2 * pp + 1) <= h) { // has one child?
			if ((*(p_person + 2 * pp + 1))->mobile > (*(p_person + pp))->mobile) { // left child bigger?
				swap(pp, 2 * pp + 1, p_person); // left (and only) child compared
			}
		}
	} // everything compared
	return parent;
}

int cmp_first(int pp, int h, nn** p_person) {
	int parent = h;
	if ((2 * pp + 2) <= h) { // has two children?
		if (0 < strcmp((*(p_person + 2 * pp + 2))->first, (*(p_person + 2 * pp + 1))->first) // right bigger than left ?
		    && 0 < strcmp((*(p_person + 2 * pp + 2))->first, (*(p_person + pp))->first)) { // AND right bigger that partent ?
			parent = swap(pp, 2 * pp + 2, p_person); // element changed, need to sort down the branch
		} else { // right NOT bigger than left and parent
			if (0 < strcmp((*(p_person + 2 * pp + 1))->first, (*(p_person + pp))->first)) { // left child bigger?
				parent = swap(pp, 2 * pp + 1, p_person); // element changed, need to sort down the branch
			}
		}
	} else { // both children compared
		if ((2 * pp + 1) <= h) { // has one child?
			if (0 < strcmp((*(p_person + 2 * pp + 1))->first, (*(p_person + pp))->first)) { // left child bigger?
				swap(pp, 2 * pp + 1, p_person); // left (and only) child compared
			}
		}
	} // everything compared
	return parent;
}

int cmp_second(int pp, int h, nn** p_person) {
	int parent = h;
	if ((2 * pp + 2) <= h) { // has two children?
		if (0 < strcmp((*(p_person + 2 * pp + 2))->second, (*(p_person + 2 * pp + 1))->second) // right bigger than left ?
		    && 0 < strcmp((*(p_person + 2 * pp + 2))->second, (*(p_person + pp))->second)) { // AND right bigger that partent ?
			parent = swap(pp, 2 * pp + 2, p_person); // element changed, need to sort down the branch
		} else { // right NOT bigger than left and parent
			if (0 < strcmp((*(p_person + 2 * pp + 1))->second, (*(p_person + pp))->second)) { // left child bigger?
				parent = swap(pp, 2 * pp + 1, p_person); // element changed, need to sort down the branch
			}
		}
	} else { // both children compared
		if ((2 * pp + 1) <= h) { // has one child?
			if (0 < strcmp((*(p_person + 2 * pp + 1))->second, (*(p_person + pp))->second)) { // left child bigger?
				swap(pp, 2 * pp + 1, p_person); // left (and only) child compared
			}
		}
	} // everything compared
	return parent;
}
