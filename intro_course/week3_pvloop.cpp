// Copyright (c) 2007 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

/*****************************************************************
 *
 *  Name:     Bartosz Szczesny
 *  username: mbcx6bs2
 *  Date:     12.10.2007
 *
 *****************************************************************/

#include <iostream>
using namespace std;

int main(void) {
	const double R = 8.315;
	char A;

	do {
		cout << "Do you wish to enter a temperature and volume? ([Y]es or [N]o)" << endl;
		cin >> A;

		if (A == 'y' || A == 'Y') {
			double volume = 0.0;
			cout << "Please enter volume (m^3)" << endl;
			cin >> volume;

			char a;
			cout << "Is the temperature in [K]elvin or [C]elsius ?" << endl;
			cin >> a;

			if (a == 'k' || a == 'K') {
				double temp_K = 0.0;
				cout << "Please enter temperature (K)" << endl;
				cin >> temp_K;

				double pressure =  R * temp_K / volume;

				cout << "One mole of gas at " << temp_K
				     << " K in volume " << volume
				     << " m^3 has a pressure of " << pressure
				     << " Pa." << endl;
			}

			if (a == 'c' || a == 'C') {
				double temp_C = 0.0;
				cout << "Please enter temperature (C)" << endl;
				cin >> temp_C;

				double pressure =  R * (temp_C + 273.15) / volume;

				cout << "One mole of gas at " << temp_C
				     << " C in volume " << volume
				     << " m^3 has a pressure of " << pressure
				     << " Pa." << endl;
			}
		} // if (A == 'y' || A == 'Y')
	} while (A == 'y' || A == 'Y'); // end of the do loop

	cout << "Thank you for using the ideal gas pressure calculator." << endl;
	return 0;
} // int main(void)
