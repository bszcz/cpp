// Copyright (c) 2007 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

/*********************************************************************************
 *
 *  Name:     Bartosz Szczesny
 *  Username: mbcx6bs2
 *  Date:     09.11.2007
 *
 *********************************************************************************/

#include <iostream>

using namespace std;

int main(void) {
	const int LENMAX = 1000;
	int len = 0;
	int arr[LENMAX] = {};
	int* arrp[LENMAX] = {};

	// get addresses of corresponding integers
	for (int i = 0; i < LENMAX; i++) {
		*(arrp + i) = (arr + i);
	}

	for (int i = 0; i < LENMAX; i++) {
		cout << "Please enter an integer (0 to end): ";
		cin >> *(arr + i);
		if (0 == *(arr + i)) {
			break;
		}
		len++;
	}

	cout << endl << "The unsorted array is:" << endl;
	for (int i = 0; i < len; i++) {
		cout << '\t' << "Element " << i << ":" << '\t' << *(*(arrp + i)) << endl;
	}

	for (int i = (len - 1); i > 0; i--) {
		for (int j = 0; j < i; j++) {
			if (**(arrp + j) > **(arrp + j + 1)) {
				int* temp = *(arrp + j);
				*(arrp + j) = *(arrp + j + 1);
				*(arrp + j + 1) = temp;
			}
		}
	}

	cout << endl << "The sorted array is:" << endl;
	for (int i = 0; i < len; i++) {
		cout << '\t' << "Element " << i << ":" << '\t' << *(*(arrp + i)) << endl;
	}

	cout << endl << "Thank you for using this pointer array sorting software." << endl;
	return 0;
} // int main(void)
