// Copyright (c) 2007 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

/*************************************************
**   Full Name: Bartosz Szczesny
**   Username : mbcx6bs2
**   Date     : 19.10.2007
*************************************************/

#include <iostream>
#include <cmath>

using namespace std;

// declaration and initialisation of global constants
const double SPEED_OF_LIGHT = 299792458.0;

// function prototype declarations
double gamma(double speed);
double kinetic_mm(double mass, double momentum);
double kinetic_ms(double mass, double speed);

int main(void) {
	char a; // anwsers to questions
	do {
		double mass = 0.0;
		cout << "Please enter the mass (kg)" << endl;
		cin >> mass;

		cout << "Are you going to enter a [S]peed or a [M]omentum ?" << endl;
		cin >> a;

		if (a == 's' || a == 'S') {
			double speed = 0.0;
			do {
				cout << "Please enter the speed (m/s)" << endl;
				cin >> speed;
				if (speed >= SPEED_OF_LIGHT) {
					cout << "The speed must be less than the speed of light." << endl;
				}
			} while (speed >= SPEED_OF_LIGHT);

			double kinetic_energy = kinetic_ms(mass, speed);

			cout << "A mass of " << mass
			     << " kg with a speed of " << speed
			     << " m/s has a kinetic energy of " << kinetic_energy
			     << " J." << endl;
		}

		if (a == 'm' || a == 'M') {
			double momentum = 0.0;
			cout << "Please enter the momentum (kgm/s)" << endl;
			cin >> momentum;

			double kinetic_energy = kinetic_mm(mass, momentum);

			cout << "A mass of " << mass
			     << " kg with a momentum of " << momentum
			     << " kgm/s has a kinetic energy of " << kinetic_energy
			     << " J." << endl;
		}

		cout << "Do you wish to perform another calculation ([Y]es or [N]o)?" << endl;
		cin >> a;

	} while (a == 'y' || a == 'Y'); // end of main the do loop

	cout << "Thank you for using the relativistic kinetic energy calculator." << endl;
	return 0;
} // int main(void)

// function definitions

double gamma(double speed) {
	return 1 / sqrt(1 - pow(speed, 2) / pow(SPEED_OF_LIGHT, 2));
}

double kinetic_mm(double mass, double momentum) {
	double total_energy = sqrt(pow(momentum, 2) * pow(SPEED_OF_LIGHT, 2) + pow(mass, 2) * pow(SPEED_OF_LIGHT, 4));
	double rest_energy = mass * pow(SPEED_OF_LIGHT, 2);
	return total_energy - rest_energy;
}

double kinetic_ms(double mass, double speed) {
	double momentum = gamma(speed) * mass * speed;
	return kinetic_mm(mass, momentum);
}
