// Copyright (c) 2007 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

/**************************************************************************************
 *
 *  Username: mbcx6bs2
 *  Name:     Bartosz Szczesny
 *  Date:     16.11.2007
 *
 *************************************************************************************/

#include <iostream>
#include <cstring>
#include <fstream>
#include <cstdlib>
#include <cmath>

using namespace std;

int main(void) {
	const int LENMAX = 1000;
	const int STRMAX = 50;

	char inpath[STRMAX] = {};
	cout << "Please enter the input filename: ";
	cin >> inpath;
	char outpath[STRMAX] = {};
	cout << "Please enter the output filename: ";
	cin >> outpath;

	// open infile and check if ok
	ifstream in(inpath);
	if (!in) {
		cerr << "Failed to open input file " << inpath << endl;
		exit(EXIT_FAILURE);
	}

	// read values from infile to arrays
	double x[LENMAX] = {};
	double y[LENMAX] = {};
	int len = 0;
	while (len < LENMAX && in >> x[len] >> y[len]) { // safe since && evaluates left-to-right
		len++;
	}
	in.close();
	cout << x[0] << " " << y[0] << endl;
	// calculate the sums
	double Sx = 0.0;
	double Sy = 0.0;
	double Sx2 = 0.0;
	double Sxy = 0.0;
	for (int i = 0; i < len; i++) {
		Sx += x[i];
		Sy += y[i];
		Sx2 += x[i] * x[i];
		Sxy += x[i] * y[i];
	}

	// main calcualtions
	double D = ((len * Sx2) - (Sx * Sx));
	double m = ((len * Sxy) - (Sx * Sy)) / D;
	double dm = sqrt(len / D);
	double c = ((Sy * Sx2) - (Sx * Sxy)) / D;
	double dc = sqrt(Sx2 / D);

	double mxpc[LENMAX] = {};
	double res[LENMAX] = {};
	double xsq = 0.0;
	// calculate mx+c, residuals and sum of residuals squared
	for (int i = 0; i < len; i++) {
		mxpc[i] = m * x[i] + c;
		res[i] = mxpc[i] - y[i];
		xsq += res[i] * res[i];
	}

	// open/create outfile and check if ok
	ofstream out(outpath);
	if (!out) {
		cerr << "Failed to open input file " << inpath << endl;
		exit(EXIT_FAILURE);
	}

	// write header to outfile
	out << "For y = mx + c, with errors on y of sigma, the line of best fit has" << endl
	    << endl << '\t' << "m = " << m << " +/- (" << dm << " * sigma)"
	    << endl << '\t' << "c = " << c << " +/- (" << dc << " * sigma)" << endl
	    << endl << "The data and the fitted points are: " << endl
	    << "\tx\ty\tmx+c\tresidual" << endl << endl;

	// write values to outfile
	out.precision(6);
	for (int i = 0; i < len; i++) {
		out << '\t' << x[i] << '\t' << y[i] << "\t" << mxpc[i] << "\t" << res[i] << endl;
	}

	// and finally, write the values of xsq to out file
	out << endl << "Chi-squared = " << xsq << " / (sigma)^2";
	out.close();

	return 0;
} // int main(void)
