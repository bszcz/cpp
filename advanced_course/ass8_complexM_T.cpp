// Copyright (c) 2010 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

// Bartosz Szczesny /19 Apr 2010/
// PHYS 30762 Programming in C++
// Assigment 8 - Advanced Topics

#include "ass8_complexH_T.hpp"
#include <cmath>

using namespace std;
using namespace cns;

int main(void) {
	cout << endl << "INTEGERS" << endl << endl;

	// two complex numbers
	complex<int> a(2, 1);
	complex<int> b(3, -4);

	// real, imaginary and conjugate (using overloading of <<)
	cout << "Complex number a = " << a << ": Re(a) = " << a.real()
	     << ", Im(a) = " << a.imag() << ", a^* = " << a.conj() << endl;
	cout << "Complex number b = " << b << ": Re(b) = " << b.real()
	     << ", Im(b) = " << b.imag() << ", b^* = " << b.conj() << endl << endl;

	// modulus  of a complex number
	cout << "Modulus  of " << a << " is " << a.mod() << endl;
	cout << "Modulus  of " << b << " is " << b.mod() << endl << endl;

	// argument of a complex number
	cout << "Argument of " << a << " is " << a.arg() << endl;
	cout << "Argument of " << b << " is " << b.arg() << endl << endl;

	cout << a << " + " << b << " = " << a + b << endl; // addition    of 2 complex numbers
	cout << a << " - " << b << " = " << a - b << endl; // subtraction of 2 complex numbers
	cout << a << " * " << b << " = " << a * b << endl; // complex multiplication (returns double)
	cout << a << " / " << b << " = " << a / b << endl; // complex division       (returns complex)

	cout << endl << "DOUBLES" << endl << endl;

	// two complex numbers
	complex<double> c(1.1, 1.2);
	complex<double> d(3.0, -4.0);

	// real, imaginary and conjugate (using overloading of <<)
	cout << "Complex number c = " << c << ": Re(c) = " << c.real()
	     << ", Im(c) = " << c.imag() << ", c^* = " << c.conj() << endl;
	cout << "Complex number d = " << d << ": Re(d) = " << d.real()
	     << ", Im(d) = " << d.imag() << ", d^* = " << d.conj() << endl << endl;

	// modulus  of a complex number
	cout << "Modulus  of " << c << " is " << c.mod() << endl;
	cout << "Modulus  of " << d << " is " << d.mod() << endl << endl;

	// argument of a complex number
	cout << "Argument of " << c << " is " << c.arg() << endl;
	cout << "Argument of " << d << " is " << d.arg() << endl << endl;

	cout << c << " + " << d << " = " << c + d << endl; // addition    of 2 complex numbers
	cout << c << " - " << d << " = " << c - d << endl; // subtraction of 2 complex numbers
	cout << c << " * " << d << " = " << c * d << endl; // complex multiplication (returns double)
	cout << c << " / " << d << " = " << c / d << endl; // complex division       (returns complex)

	return 0;
}
