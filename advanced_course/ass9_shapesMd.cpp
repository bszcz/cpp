// Copyright (c) 2010 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

// Bartosz Szczesny /23 Apr 2010/
// PHYS 30762  Programming in C++
// Assignment 9 - Project: Shapes

#include "shapesH.hpp"
using namespace std;
using namespace shp;

int main() {
	vector<Shape<>* > vec;

	char op;
	do {
		cout << "\nMAIN MENU\n"
		     << "\n p -> Print shapes."
		     << "\n a -> Add   shape."
		     << "\n e -> Erase shape."
		     << "\n q -> Quit.\n"
		     << "\nPlease choose an option: ";
		cin >> op;

		switch (op) {
		case 'a':
			vec = add_shape(vec);
			break;
		case 'e':
			vec = erase_shape(vec);
			break;
		case 'p':
			print_shapes(vec);
			break;
		case 'q':
			cout << "\nBye.\n\n";
			break;
		default:
			cout << "\nSorry, didn't quite catch that. Could you try again please?\n";
		}

	} while (op != 'q');

	return 0;
}
