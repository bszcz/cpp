// Copyright (c) 2010 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

// Bartosz Szczesny /23 Apr 2010/
// PHYS 30762  Programming in C++
// Assignment 9 - Project: Shapes

#ifndef SHAPES_H  // pre-processor directive
#define SHAPES_H  // class defined only once

#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <cmath>
using namespace std;

namespace shp { // use by adding "using namespace shp;"

const double PI(3.141592654); // define global constant

//
// SHAPE CLASS: NAME & INTERFACE
//

template <class T = double>
class Shape { // general shape class with virtual interface
protected:
	string typ;  // shape name as a type

public:
	Shape()            : typ("Undefined") {}
	Shape(string typ_) : typ(typ_) {}
	~Shape() {}

	string type()  const { return typ; }  //output type
	virtual T perim() const = 0; //
	virtual T area()  const = 0; // pure virtual functions
	virtual T vol()   const = 0; //
};

//
// CORRECTION & CONVERSION
//

double posit(double value);           // corrects unphysical entries
double ang2r(double ang_, char unit); // converts angle into radians

//
// DATA STORAGE: POLAR & SIDES
//

class Polar { // storage of radius and angle
protected:
	double rad;
	double ang;

public:
	Polar()                                 : rad(0.),          ang(0.)             {}
	Polar(double rad_, double ang_, char u) : rad(posit(rad_)), ang(ang2r(ang_, u)) {}
	~Polar() {}

	void set_rad(double rad_)         { rad = posit(rad_); }
	void set_ang(double ang_, char u) { ang = ang2r(ang_, u); }

	double get_rad() const { return rad; }
	double get_ang() const { return ang; }
};

/////

template <class T = double>
class Sides { // dynamic storage of lengths
protected:
	T*  Sdat;
	int S;

public:
	Sides()       { S = 0;   Sdat = NULL; }
	Sides(int S_) { S = S_;  Sdat = new T[S]; for (int s(0); s < S; s++) Sdat[s] = 0; }
	~Sides()      { delete[] Sdat; }

	int inS(int s) const {
		if (s >= 0 && s < S) {
			return 1; // is s in range [0, S-1] ??
		} else {
			cout << "\nERROR: Index out of range.\n";  return 0;
		}
	}

	T     get_side(int s)   const { if (inS(s - 1)) return Sdat[s - 1];  else return 0; }
	void  set_side(int s, T side) { if (inS(s - 1))        Sdat[s - 1] = posit(side); }
};

//
// SHAPES: SIDES TYPE
//

class Triangle: public Shape<>, public Sides<> { // multiple inheritance
public:
	Triangle() : Shape<>("Triangle"), Sides<>(3) {}
	Triangle(double side1, double side2, double side3); // PROTOTYPE
	~Triangle() {}

	double perim() const { return Sides<>::Sdat[0] + Sides<>::Sdat[1] + Sides<>::Sdat[2]; }
	double area()  const {
		double p = perim() / 2.; // half perimeter, needed for Heron's formula below
		return sqrt(p * (p - Sides<>::Sdat[0]) * (p - Sides<>::Sdat[1]) * (p - Sides<>::Sdat[2]));
	}
	double vol()   const { return 0; }
};

/////

template <class T = double>
class Rectangle: public Shape<T>, public Sides<T> { // multiple inheritance
public:
	Rectangle()                 : Shape<T>("Rectangle"), Sides<T>(2) {}
	Rectangle(T side1, T side2) : Shape<T>("Rectangle"), Sides<T>(2) {
		set_side(1, side1); set_side(2, side2);
	}
	~Rectangle() {}

	T perim() const { return 2 * Sides<T>::Sdat[0] + 2 * Sides<T>::Sdat[1]; } // need Sides<T>:: to call
	T area()  const { return Sides<T>::Sdat[0] * Sides<T>::Sdat[1]; } // variables from Sides<T>
	T vol()   const { return 0; }
};

/////

template <class T = double>
class Cuboid: public Shape<T>, public Sides<T> { // multiple inheritance
public:
	Cuboid()                          : Shape<T>("Cuboid"), Sides<T>(3) {}
	Cuboid(T side1, T side2, T side3) : Shape<T>("Cuboid"), Sides<T>(3)
	{ set_side(1, side1);  set_side(2, side2);  set_side(3, side3); }
	~Cuboid() {}

	T perim() const { return 4 * Sides<T>::Sdat[0] + 4 * Sides<T>::Sdat[1] + 4 * Sides<T>::Sdat[2]; }
	T area()  const {
		return 2 * Sides<T>::Sdat[0] * Sides<T>::Sdat[1] +
		       2 * Sides<T>::Sdat[1] * Sides<T>::Sdat[2] +
		       2 * Sides<T>::Sdat[2] * Sides<T>::Sdat[0];
	}
	T vol()   const { return Sides<T>::Sdat[0] * Sides<T>::Sdat[1] * Sides<T>::Sdat[2]; }
};



//
// SHAPES: POLAR TYPE
//

class fCircle: public Shape<>, public Polar { // multiple inheritance
public:
	fCircle()                                  : Shape<>("fCircle"), Polar() {}
	fCircle(double rad, double ang, char unit) : Shape<>("fCircle"), Polar(rad, ang, unit) {}
	~fCircle() {}

	double perim() const {
		if (Polar::ang == 2 * PI) {
			return Polar::ang * Polar::rad;
		} else {
			return (2 + Polar::ang) * Polar::rad;
		}
	}
	double area()  const { return Polar::ang * Polar::rad * Polar::rad / 2.; }
	double vol()   const { return 0; }
};

/////

class fSphere: public Shape<>, public Polar {
public:
	fSphere()                                  : Shape<>("fSphere"), Polar() {}
	fSphere(double rad, double ang, char unit) : Shape<>("fSphere"), Polar(rad, ang, unit) {}
	~fSphere() {}

	double perim() const { return 0; }
	double area()  const {
		if (Polar::ang == 2 * PI) {
			return Polar::ang * 2. * Polar::rad * Polar::rad;
		} else {
			return (PI + Polar::ang * 2.) * Polar::rad * Polar::rad;
		}
	}
	double vol()   const { return Polar::ang * 2. * Polar::rad * Polar::rad * Polar::rad / 3.; }
};

//
// PRISM
//

class Prism:  public Shape<> {
private:
	double depth;
	double b_area, b_perim; // b == base

public:
	Prism() : Shape<>("Undefined Prism") { b_perim = 0;  b_area = 0; depth = 0.; }

	Prism(Triangle b, double depth_) : Shape<>(b.type() + " Prism")
	{ b_perim = b.perim();  b_area = b.area();  depth = depth_; }
	Prism(fCircle  b, double depth_) : Shape<>(b.type() + " Prism")
	{ b_perim = b.perim();  b_area = b.area();  depth = depth_; }

	Prism(Rectangle<int> b, double depth_) : Shape<>(b.type() + " Prism")
	{ b_perim = b.perim();  b_area = b.area();  depth = depth_; }
	Prism(Rectangle<>    b, double depth_) : Shape<>(b.type() + " Prism")
	{ b_perim = b.perim();  b_area = b.area();  depth = depth_; }
	~Prism() {}

	double perim() const { return 0; }
	double area()  const { return b_perim * depth + 2. * b_area; }
	double vol()   const { return b_area * depth; }
};

// MAIN PROGRAM INTERFACE
void print_shapes(vector<Shape<>* > &vec);
vector<Shape<>* >   add_shape(vector<Shape<>* > &vec);
vector<Shape<>* >   add_prism(vector<Shape<>* > &vec);
vector<Shape<>* > erase_shape(vector<Shape<>* > &vec);

} // end of shp namespace

#endif // end of pre-processor directive
