// Copyright (c) 2010 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

// Bartosz Szczesny /05 Mar 2010/
// PHYS 30762 Programming in C++
// Assignment 5 - A matrix class

#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

class matrix {
private:
	double* mdat;
	int rows;
	int cols;

public:
	matrix() {
		mdat = NULL;
		rows = 0;
		cols = 0;
	}
	matrix(int n, int m); // constructor: parameterised
	matrix(const matrix& mat); // constructor: copy
	~matrix() {
		delete[] mdat;
	}

	int isempty() const {
		if (mdat == NULL) {
			cout << "error: empty array" << endl;
			return 1;
		} else {
			return 0;
		}
	}
	int inrange(int n, int m) const {
		if (n > 0 && n <= rows && m > 0 && m <= cols) {
			return 1;
		} else  {
			cout << "error: index (" << n << "," << m << ") out of range for "
			     << rows << "x" << cols << " matrix." << endl;
			return 0;
		}
	}
	int is2x2() const {
		if (rows == 2 && cols == 2) {
			return 1;
		} else  {
			cout << "error: matrix is not 2x2." << endl;
			return 0;
		}
	}

	int getrows() const {
		return rows;
	}
	int getcols() const {
		return cols;
	}
	int row(int i) const {
		return (i - i % cols) / cols + 1;
	}
	int col(int i) const {
		return i % cols + 1;
	}

	int index(int n, int m) const {
		if (inrange(n, m) && !isempty()) {
			return (n - 1) * cols + (m - 1);
		} else {
			return -1; // instead of exit(EXIT_FAILURE)
		}
	}

	double getval(int n, int m) const {
		if (inrange(n, m) && !isempty()) {
			return mdat[index(n, m)];
		} else {
			return 0; // instead of exit(EXIT_FAILURE)
		}
	}
	void setval(int n, int m, double val) {
		if (inrange(n, m) && !isempty()) {
			mdat[index(n, m)] = val;
		}
	}

	matrix operator=(const matrix& mat);
	matrix operator+(const matrix& mat) const {
		return genadd(*this, mat, +1);
	}
	matrix operator-(const matrix& mat) const {
		return genadd(*this, mat, -1);
	}
	matrix operator*(const matrix& mat) const;
	friend ostream& operator<<(ostream& os, const matrix& mat);

	// need for +/- overloading
	friend matrix genadd(const matrix& m1, const matrix& m2, int s);

	matrix trans() const {
		matrix mT(cols, rows);
		for (int n = 1; n <= rows; n++) {
			for (int m = 1; m <= cols; m++) {
				mT.setval(m, n, (*this).getval(n, m));
			}
		}
		return mT;
	}
	double det() const {
		if (is2x2() && !isempty()) {
			return mdat[0] * mdat[3] - mdat[1] * mdat[2];
		} else {
			return 0.0;
		}
	}
	double tr() const; // trace, for square matrix only
	void evals() const; // eigenvalues, for 2x2 matrix only
	matrix inv() const; // inverse, for 2x2 matrix only

}; // class matrix

// constructor: parameterised
matrix::matrix(int n, int m) {
	if (n > 0 && m > 0) {
		rows = n;
		cols = m;
		mdat = new double[rows * cols];
		for (int i = 0; i < rows * cols; i++) {
			mdat[i] = 0.0;
		}
	} else  {
		mdat = NULL;
		rows = 0;
		cols = 0;
	}
}

matrix::matrix(const matrix& mat) {
	mdat = NULL;
	rows = mat.getrows();
	cols = mat.getcols();
	if (rows > 0 && cols > 0) {
		mdat = new double[rows * cols];
		for (int n = 1; n <= rows; n++) {
			for (int m = 1; m <= cols; m++) {
				(*this).setval(n, m, mat.getval(n, m));
			}
		}
	}
}

ostream& operator<<(ostream& os, const matrix& mat) {
	os << endl;
	if (!mat.isempty()) {
		int rows = mat.getrows();
		int cols = mat.getcols();
		for (int n = 1; n <= rows; n++) {
			os << "[ ";
			for (int m = 1; m <= cols; m++) {
				os << setw(3) << mat.getval(n, m) << " ";
			}
			os << "]" << endl;
		}
	}
	return os;
}

matrix matrix::operator=(const matrix& mat) {
	delete[] mdat;
	mdat = NULL;
	rows = mat.getrows();
	cols = mat.getcols();
	if (rows > 0 && cols > 0) {
		mdat = new double[rows * cols];
		for (int n = 1; n <= rows; n++) {
			for (int m = 1; m <= cols; m++) {
				(*this).setval(n, m, mat.getval(n, m));
			}
		}
	}
	return *this;
}

// general addition (overloading both + and - operators)
matrix genadd(const matrix& m1, const matrix& m2, int sign) {
	int rows = m1.getrows();
	int cols = m1.getcols();
	if (rows != m2.getrows() || cols != m2.getcols()) {
		cout << "error: adding matrices of different dimensions" << endl;
		matrix m3;
		return m3;
	} else {
		matrix m3(rows, cols);
		for (int n = 1; n <= rows; n++) {
			for (int m = 1; m <= cols; m++) {
				m3.setval(n, m, m1.getval(n, m) + sign * m2.getval(n, m));
			}
		}
		return m3;
	}
}

matrix matrix::operator*(const matrix& mat) const {
	if (cols != mat.getrows()) {
		cout << "error: multiplying matrices of incorrect dimensions" << endl;
		matrix m3;
		return m3;
	} else {
		matrix m3(rows, mat.getcols());
		for (int n = 1; n <= rows; n++) {
			for (int m = 1; m <= mat.getcols(); m++) {
				double m3nm(0.);
				for (int l = 1; l <= cols; l++) {
					m3nm += (*this).getval(n, l) * mat.getval(l, m);
				}
				m3.setval(n, m, m3nm);
			}
		}
		// "chop" values close to 0.0
		for (int n = 1; n <= rows; n++) {
			for (int m = 1; m <= mat.getcols(); m++) {
				if (m3.getval(n, m) < 10e-6) {
					m3.setval(n, m, 0.0);
				}
			}
		}
		return m3;
	}
}

double matrix::tr() const {
	double tr(0);
	if (rows == cols) {
		for (int n = 1; n <= rows; n++) {
			tr += (*this).getval(n, n);
		}
	} else {
		cout << "error: not a square matrix" << endl;
	}
	return tr;
}

void matrix::evals() const {
	if (is2x2() && !isempty()) {
		double disc = (*this).tr() * (*this).tr() - 4.0 * (*this).det();
		if (disc < 0) {
			cout << "error: complex eigenvalues" << endl;
		} else {
			double ev1 = ((*this).tr() + sqrt(disc)) / 2.0;
			double ev2 = ((*this).tr() - sqrt(disc)) / 2.0;
			cout << ev1 << ", " << ev2 << endl;
		}
	}
}

matrix matrix::inv() const {
	if (is2x2() && !isempty()) {
		double d = (*this).det();
		if (d == 0)  {
			cout << "error: singluar matrix" << endl;
			return *this;
		} else {
			matrix m_inv(2, 2);
			m_inv.setval(1, 1, +(*this).getval(2, 2) / d);
			m_inv.setval(2, 2, +(*this).getval(1, 1) / d);
			m_inv.setval(1, 2, -(*this).getval(1, 2) / d);
			m_inv.setval(2, 1, -(*this).getval(2, 1) / d);
			return m_inv;
		}
	} else {
		return *this;
	}
}

int main(void) {
	matrix a1; // constructor: default
	cout << "Matrix a1:" << a1; // error: empty array

	const int n = 2;
	const int m = 2;
	matrix a2(n, m); // constructor: parameterised

	// set values for a2
	a2.setval(1, 2, 1.0);
	a2.setval(1, 3, 2.0); // error: index out of range
	a2.setval(2, 1, 3.0);
	a2.setval(2, 2, 4.0);
	a2.setval(2, 3, 5.0); // error: index out of range

	// print matrix a2
	cout << "Matrix a2:" << a2;

	matrix a3(n, m);
	cout << "Matrix a3:" << a3;
	a3 = a2; // deep copy: assignment
	cout << "Matrix a3:" << a3;
	matrix a4(a2); // deep copy: copy constructor
	cout << "Matrix a4:" << a4;

	// modify matrix a2
	a2.setval(1, 1, 1.0);
	a2.setval(1, 2, 3.0);
	cout << "Matrix a2 modified." << endl;

	// print after modifying matrix a2
	cout << "Matrix a2:" << a2;
	cout << "Matrix a3:" << a3;
	cout << "Matrix a4:" << a4;

	// matrix operations
	cout << "Matrix a2+a4:" << a2 + a4;
	cout << "Matrix a2-a4:" << a2 - a4;
	cout << "Matrix a2 * a4^T:" << a2* a4.trans();
	cout << "Matrix a4^T:" << a4.trans();
	cout << "Matrix a4 determinant: " << a4.det() << endl;
	cout << "Matrix a4 trace: " << a4.tr() << endl;
	cout << "Matrix a4 eigenvalues: ";
	a4.evals();
	cout << "Matrix a2 * a2^-1:" << a2* a2.inv() << endl;

	return 0;
}
