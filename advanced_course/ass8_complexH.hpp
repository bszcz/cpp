// Copyright (c) 2010 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

// Bartosz Szczesny /19 Apr 2010/
// PHYS 30762 Programming in C++
// Assigment 8 - Advanced Topics

#ifndef COMPLEX_H  // pre-processor directive
#define COMPLEX_H  // class defined only once

#include <iomanip>
#include <iostream>

using namespace std;

namespace cns { // REMEMBER TO ADD unsing namespace cns;

class complex {
private:
	double re, im;

public:
	// constructors & destructor
	complex()                   { re = 0; im = 0; }
	complex(double r, double i) { re = r; im = i; }
	~complex() {}

	complex conj() const; // complex conjugate

	double real() const; // Re part
	double imag() const; // Im part
	double mod()  const; // modulus
	double arg()  const; // argument

	complex operator+(const complex &z) const;  //
	complex operator-(const complex &z) const;  // OVER
	complex operator*(const complex &z) const;  // LOAD
	complex operator/(const complex &z) const;  //

	friend ostream& operator<<(ostream &os, const complex &z); // generates output
};  // end of complex class

ostream& operator<<(ostream &os, const complex &z); // need outside class declaration

} // end of cns namespace

#endif // end of pre-processor directive
