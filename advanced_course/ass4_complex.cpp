// Copyright (c) 2010 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

// Bartosz Szczesny /23 Feb 2010/
// PHYS 30762 Programming in C++
// Class for complex numbers

#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

class complex {
private:
	double re;
	double im;

public:
	complex() {
		re = 0.0;
		im = 0.0;
	}
	complex(double re_, double im_) {
		re = re_;
		im = im_;
	}
	~complex() {}

	double real() const {
		return re;
	}
	double imag() const {
		return im;
	}

	complex conj() const {
		double c_im = (-1.0) * im;
		if (im == 0.0) {
			c_im = 0.0; // avoiding "-0"
		}
		complex c(re, c_im);
		return c;
	}

	double mod() const {
		return sqrt(re * re + im * im);
	}
	double arg() const {
		double a = 0;
		if (re == 0 && im == 0) {
			cout << "ERROR: argument of (0 + 0j) undefined" << endl;
		} else {
			a = atan2(im, re); // atan2(y,x) <- gives correct values for x = 0
		}
		return a;
	}

	// OVERLORDING:
	complex operator+(const complex& z) const {
		complex c(re + z.real(), im + z.imag());
		return c;
	}
	complex operator-(const complex& z) const {
		complex c(re - z.real(), im - z.imag());
		return c;
	}
	complex operator*(const complex& z) const {
		double c_re = re * z.real() - im * z.imag(); // x1*x2 - y1*y2
		double c_im = re * z.imag() + im * z.real(); // x1*y2 + y1*x2
		complex c(c_re, c_im);
		return c;
	}
	complex operator/(const complex& z) const {
		if (z.mod() == 0) {
			cout << "ERROR: division by (0 + 0j)" << endl;
			complex c;
			return c;
		} else {
			double z_mod_sq = z.mod() * z.mod();
			complex z2(re / z_mod_sq, im / z_mod_sq);
			return z2 * z.conj();
		}

	}

	// friend: << operator overload
	friend ostream& operator<<(ostream& os, const complex& z);
}; // class complex

// function: << operator overload (complex class friend)
ostream& operator<<(ostream& os, const complex& z) {
	if (z.imag() >= 0) {
		os << setprecision(3) << "(" << z.real() << " + " <<  z.imag() << "j)";
	} else {
		os << setprecision(3) << "(" << z.real() << " - " << -z.imag() << "j)";
	}
	return os;
}

int main(void) {
	complex a(3, 4);
	complex b(2, -8);

	cout << "a = " << a << ": Re(a) = " << a.real() << ", Im(a) = " << a.imag() << ", Conj(a) = " << a.conj() << endl;
	cout << "b = " << b << ": Re(b) = " << b.real() << ", Im(b) = " << b.imag() << ", Conj(b) = " << b.conj() << endl;

	cout << "Modulus  of " << a << " is " << a.mod() << endl;
	cout << "Modulus  of " << b << " is " << b.mod() << endl;
	cout << "Argument of " << a << " is " << a.arg() << endl;
	cout << "Argument of " << b << " is " << b.arg() << endl;

	cout << a << " + " << b << " = " << a + b << endl;
	cout << a << " - " << b << " = " << a - b << endl;
	cout << a << " * " << b << " = " << a * b << endl;
	cout << a << " / " << b << " = " << a / b << endl;

	return 0;
}
