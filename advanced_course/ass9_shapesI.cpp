// Copyright (c) 2010 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

// Bartosz Szczesny /23 Apr 2010/
// PHYS 30762  Programming in C++
// Assignment 9 - Project: Shapes

#include "shapesH.hpp"
using namespace std;
using namespace shp;

//
// CORRECTION & CONVERSION
//

double shp::posit(double value) { // corrects unphysical entries
	if (value >  0) {
		return value;
	} else if (value == 0) {
		cout << "\nError: Zero value. Unity assumed.\n";
		return 1;
	} else {
		cout << "\nError: Uphysical value. Modulus assumed.\n";
		return -value;
	}
}

/////

double shp::ang2r(double ang_, char unit) { // converts angle into radians
	double ang;
	ang_ = posit(ang_);

	switch (unit) {
	case 'r':
		ang = ang_; // radians
		break;
	case 'd':
		ang = ang_ / 360.*2.*PI; // degrees
		break;
	case 'f':
		ang = ang_ * 2.*PI; // fraction
		break;
	default :
		ang = ang_;
		cout << "\nError: Incorrect unit of angle measure. Radians assumed.\n";
	}

	if (ang <= 0 || ang > 2.*PI) {
		cout << "\nError: Angle not between 0 and 2*Pi. Value of 2*Pi assumed.\n";
		ang = 2.*PI;
	}

	return ang;
}

//
// TRIANGLE CONSTRUCTOR
//

shp::Triangle::Triangle(double side1, double side2, double side3) : Shape<>("Triangle"), Sides<>(3) {
	side1 = posit(side1);
	side2 = posit(side2);
	side3 = posit(side3);

	// normalises sides if incorrect are given
	if ((side1 + side2 < side3) || (side2 + side3 < side1) || (side3 + side1 < side2)) {
		cout << "\nError: Cannot form triangle of given sides. Unit sides assumed.\n";
		side1 = 1;
		side2 = 1;
		side3 = 1;
	}

	// set sides of triangle
	set_side(1, side1);
	set_side(2, side2);
	set_side(3, side3);
}

//
// MAIN PROGRAM INTERFACE
//

void shp::print_shapes(vector<Shape<>* > &vec) {
	cout << "\nPRINTING SHAPES\n";

	if (!vec.size()) {
		cout << "\n The array with shapes is actually empty."
		     << "\n Why not add shapes with the 'a' option ?\n";
	}

	for (int n(0); n < vec.size(); n++) {
		cout << "\n Shape No : " << n + 1
		     << "\n  Name      : " << vec[n]->type()
		     << "\n  Perimeter : " << vec[n]->perim()
		     << "\n  Area      : " << vec[n]->area()
		     << "\n  Volume    : " << vec[n]->vol()
		     << "\n";
	}
}

/////

vector<Shape<>* > shp::add_prism(vector<Shape<>* > &vec) {
	int no;
	double dummy1, dummy2, dummy3, depth;
	char type;

	cout << "\nPlease enter the depth of the prism: ";
	cin >> depth;

	cout << "\nChoose prism base [ (r)rectangle, (t)riangle, (c)ricle ]: ";
	cin >> type;

	switch (type) {
	case 'r':
		cout << "\n Please enter side1, side2: ";
		cin >> dummy1 >> dummy2;
		vec.push_back(new Prism(Rectangle<>(dummy1, dummy2), depth));
		break;
	case 't':
		cout << "\n Please enter side1, side2, side2: ";
		cin >> dummy1 >> dummy2 >> dummy3;
		vec.push_back(new Prism(Triangle(dummy1, dummy2, dummy3), depth));
		break;

	case 'c':
		cout << "\n Please enter radius, angle, angle-unit [(r)ad, (d)eg, (f)raction]: ";
		cin >> dummy1 >> dummy2 >> type;
		vec.push_back(new Prism(fCircle(dummy1, dummy2, type), depth));
		break;

	default :
		cout << "\n No prism was created." << endl;
	}

	return vec;
}

/////

vector<Shape<>* > shp::add_shape(vector<Shape<>* > &vec) {
	char type, cont;
	double dummy1, dummy2, dummy3;

	cout << "\nADDING SHAPE\n"
	     << "\n r -> Rectangle"
	     << "\n t -> Triangle"
	     << "\n b -> Cuboid"
	     << "\n c -> fCircle"
	     << "\n s -> fSphere"
	     << "\n p -> Prism\n"
	     << "\nPlease enter shape type: ";
	cin >> type;

	switch (type) {
	case 'r':
		cout << "\n Please enter side1, side2: ";
		cin >> dummy1 >> dummy2;
		vec.push_back(new Rectangle<>(dummy1, dummy2));
		break;

	case 't':
		cout << "\n Please enter side1, side2, side3: ";
		cin >> dummy1 >> dummy2 >> dummy3;
		vec.push_back(new Triangle(dummy1, dummy2, dummy3));
		break;

	case 'b':
		cout << "\n Please enter side1, side2, side3: ";
		cin >> dummy1 >> dummy2 >> dummy3;
		vec.push_back(new Cuboid<>(dummy1, dummy2, dummy3));
		break;

	case 'c':
		cout << "\n Please enter radius, angle, angle-unit [(r)ad, (d)eg, (f)raction]: ";
		cin >> dummy1 >> dummy2 >> type;
		vec.push_back(new fCircle(dummy1, dummy2, type));
		break;

	case 's':
		cout << "\n Please enter radius, angle, angle-unit [(r)ad, (d)eg, (f)raction]: ";
		cin >> dummy1 >> dummy2 >> type;
		vec.push_back(new fSphere(dummy1, dummy2, type));
		break;

	case 'p':
		vec = add_prism(vec);
		break;

	default :
		cout << "\n Sorry, only five shapes are known. Could you try again please?\n";
	}

	return vec;
} // end of add_shape function

/////

vector<Shape<>* > shp::erase_shape(vector<Shape<>* > &vec) {
	cout << "\nERASING SHAPES\n";

	if (!vec.size()) {
		cout << "\n Sorry, cannot erase any shapes, the array is empty.";
	} else {
		cout << "\n Please enter the shape number to erase (or 0 to cancel): ";
		int no;
		cin >> no;

		if (no) { vec.erase(vec.begin() + (no - 1)); }   // note the use of "vec.begin()"
		else { cout << "\n Having problems deciding? Don't worry, nothing was erased."; }
	}

	return vec;
}

///// END
