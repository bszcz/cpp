// Copyright (c) 2010 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

// Bartosz Szczesny /15 Feb 2010/
// PHYS 30762 Programming in C++
// Assignment 3
// Simple demonstration of a C++ class

#include <iostream>
#include <cstring>
#include <cmath>

using namespace std;

class galaxy {
private:
	string hubble_type; // Hubble types: E[0-7], S0, S[a-c], SB[a-c], Irr
	double total_mass; // total mass M_tot in range [1e7,1e12] M_Sun
	double stellar_mass_frac; // stellar mass fraction f_* in range [0,0.05]
	double redshift; // redshift z in range [0,10]

public:
	// constructor - default
	galaxy() {
		hubble_type = "Undefined";
		total_mass = 0;
		stellar_mass_frac = 0;
		redshift = 0;
	}

	// constructor - parameterised
	galaxy(string hubble_type_, double total_mass_, double stellar_mass_frac_, double redshift_) {
		hubble_type = hubble_type_;
		total_mass = total_mass_;
		stellar_mass_frac = stellar_mass_frac_;
		redshift = redshift_;
	}

	// destructor
	~galaxy() {
		cout << "Destroying: " << hubble_type << endl;
	}

	double stellar_mass() {
		return stellar_mass_frac * total_mass;
	}

	void change_type(string new_hubble_type) {
		hubble_type = new_hubble_type;
	}

	// prototype: object data print out
	void print_data();

};

// function: object data print out
void galaxy::print_data() {
	cout << "GALAXY DATA" << endl;
	cout << "\tHubble type:  " << hubble_type << endl;
	cout << "\tTotal  mass:  " << total_mass << " M_Sun" << endl;
	cout << "\tStellar mass" << endl;
	cout << "\tfraction (f): " << stellar_mass_frac << endl;
	cout << "\tRedshift (z): " << redshift << endl;
}

int main(void) {
	galaxy g1; // object using constructor - default
	galaxy g2("Irr", 1e10, 0.01, 1); // object using constructor - parameterised
	g1.print_data();
	g2.print_data();

	cout << "Stellar mass: " << g2.stellar_mass() << " M_Sun" << endl;

	cout << "Hubble type change: Irr => S0." << endl;
	g2.change_type("S0");
	g2.print_data();

	const int ndata = 3;
	galaxy g_array[ndata] = {
		galaxy("E1", 2e10, 0.02, 2),
		galaxy("E2", 3e10, 0.03, 3),
		galaxy("E3", 4e10, 0.04, 4)
	};
	cout << "Created: array of galaxy type objects." << endl;

	galaxy* g = &g_array[0]; // galaxy-array pointer
	for (int i = 0; i < ndata; i++) {
		(*g).print_data();
		g++;
	}

	return 0;
}
