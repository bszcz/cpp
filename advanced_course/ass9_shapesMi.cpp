// Copyright (c) 2010 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

// Bartosz Szczesny /23 Apr 2010/
// PHYS 30762  Programming in C++
// Assignment 9 - Project: Shapes

#include "shapesH.hpp"
#include <cstdlib>
#include <ctime>
using namespace std;
using namespace shp;

int r10(void) {
	return rand() % 10 + 1; // random integers from 1 to 10
}

int main() {
	const int N(20);
	srand(time(NULL)); // seed the random number generator
	vector<Shape<int>* > vec;

	// randomly create N rectangles and N cuboids with sides of "int" type
	for (int i(0); i < N; i++) {
		vec.push_back(new Rectangle<int>(r10(), r10()));
	}
	for (int j(0); j < N; j++) {
		vec.push_back(new Cuboid<int>(r10(), r10(), r10()));
	}

	// print shapes in array
	for (int n(0); n < 2 * N; n++) {
		cout << "\n Shape No : " << n + 1           << "\tName : " << vec[n]->type()
		     << "\tPerimeter : " << vec[n]->perim() << "\tArea : " << vec[n]->area()
		     << "\tVolume : "    << vec[n]->vol()   << "\n";
	}

	cout << endl;
	return 0;
}
