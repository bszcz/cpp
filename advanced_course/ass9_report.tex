% Copyright (c) 2010 Bartosz Szczesny
% LICENSE: The MIT License (MIT)

% C++ Proj: Shapes
% Bartosz Szczesny

\documentclass{article}
\usepackage[top=4cm,right=2.5cm,left=2.5cm,bottom=1cm]{geometry}
\usepackage{amsfonts}
\usepackage{multicol}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{comment} 
\usepackage{units}
\usepackage{alltt} 
\setlength{\parindent}{0mm}
\setlength{\columnsep}{20pt}
\setlength{\footnotesep}{5mm}
\setlength{\skip\footins}{5mm}
\renewcommand{\ttdefault}{txtt}
\newcommand{\ptab}{\hspace{3mm}}
\newcommand{\veq}{\vspace{-1mm}}
\newcommand{\veqq}{\vspace{-2mm}}
\newcommand{\veqqq}{\vspace{-3mm}}
\newcommand{\veqqqq}{\vspace{-4mm}}
\newcommand{\veqqqqq}{\vspace{-5mm}}
\begin{document}

\begin{center}

\large{\textbf{Programming in C++}}

\large{\textbf{Final Project: Shapes}}
\linebreak

Bartosz Szczesny

\textit{School of Physics and Astronomy,}
 
\textit{The University of Manchester, UK}
\end{center}

A computer code, designed for storage and manipulation of geometric shapes, will be presented in this report. The program is written in an object oriented approach using features such as multiple inheritance, templates and polymorphism. Various types of shapes can be stored in an array and their perimeters, areas and volumes are calculated by the program. The user interface consists of a menu of options which enable printing, adding and erasing shapes from the array.
\\

\begin{multicols*}{2}

\textbf{Contents}
\\

\textbf{1.} Introduction

\textbf{2.} Design and Implementation

\textbf{3.} Operation and Results

\textbf{4.} Discussion and Conclusion

Attached: C++ Source Codes
\\
~
\\

\textbf{1. Introduction}
\\

\ptab The aim of the project is to design a program capable of storing various geometric shapes in an array based on the {\tt vector} class from the Standard Template Library (STL). The code should calculate perimeters, areas and volumes of different shapes and print them onto the screen. Any two dimensional object should be convertible into a three dimensional prism by assigning a depth to it. The  member functions of the shapes should be accessible through a common abstract class and their pure virtual functions.
\\

\textbf{2. Design and Implementation}
\\

\ptab The main class of the program is the {\tt Shape$\langle\rangle$} class. It stores the basic information about the shape (their type) and provides pure virtual functions which act as an interface for all other classes derived from {\tt Shape$\langle\rangle$}. The type of the shape is stored in a variable of {\tt string} class from STL with the default being set to "Undefined". The data type used in the class is {\tt double} but additional functionality was added to enable use of {\tt int} as will be discussed later.

\ptab Two other main classes are used to store data about the shapes. There is the {\tt Polar} class which enables manipulation on radii and angles. The input data is appropriately corrected by the class and the angles can be converted from degrees and full angle fractions into radians. The radius is also corrected for negative entries. The second class called {\tt Sides$\langle\rangle$} is using a dynamic memory allocation to store a list of line segments. These can be both of {\tt double} and {\tt int} type as will be discussed in later sections. The input is again corrected for unphysical negative entries and the data is erased at the end of the execution to prevent memory leaks.

\ptab The classes which hold shapes are multiply derived from {\tt Shape$\langle\rangle$} and from either {\tt Polar} or {\tt Sides$\langle\rangle$} classes. For that reason, more than one constructor is called upon object initialisation. This enables creation of objects with unified interface of the generic {\tt Shape$\langle\rangle$} class and storage capabilities of the two data classes without having to specify any variables inside the proper shape classes. 

\ptab {\tt Polar} type classes include {\tt fCircle} and {\tt fSphere}. The prefix 'f' means that they can be defined as fractional objects, enabling manipulations on shapes such as circle segments or hemi-spheres. Shapes which consist of line segments are derived with the {\tt Sides$\langle\rangle$} class and include {\tt Triangle}, {\tt Rectangle$\langle\rangle$} and {\tt Cuboid$\langle\rangle$} classes. The latter two have an additional capability of storing their dimensions as integers. The {\tt Triangle} class uses the Heron formula which enables calculation of area from triangle sides.

\ptab In addition to shapes described above, the code can also manipulate three dimensional object of type {\tt Prism}. These can be created by adopting any two dimensional shape as a base and adding a depth to it. The program can then calculate both area and the volume of the prism by accessing the information about the shape used for the base.

\ptab All shapes are stored in a dynamic array of {\tt vector$\langle$Shape$\langle\rangle$*$\rangle$} type which enables adding and erasing object without specifying their number at the start of the program. It also allows the access to the data through the pure virtual functions displaying the perimeter, area and volume. Two dimensional objects do not have volume and therefore a value of zero is return by the program. Likewise, for the three dimensional shapes, their perimeter is set to zero. These definitions can be modified and one example is provided by displaying the length of the edges of a cuboid as its perimeter.

\ptab The code is divided into three files, the header, the implementation file and the main program. The header holds all classes definitions in a namespace {\tt shp} and is protected by a pre-processor directive to avoid multiple class instatiations. The menu functions are mentioned as prototypes and are defined in the implementation file with other auxiliary functions such as angle conversion routine.

\ptab As mentioned before, any unphysical input into the program is corrected. If a negative value is provided its modulus is used in the calculations to avoid what is most likely a sign error. Values of zero are changed to unity. The angles should be greater than 0 degrees but should not exceed the angle of 360 degrees. Negative entries are again corrected as described before. There is also a routine which ensures construction of correct triangles. If a sum of any two sides is less than the third length, a triangle with three unit sides is returned. Also any negative lengths provided are changed before constructing the triangle.
\\

\textbf{3. Operation and Results}
\\

\veqq \ptab Two main computer codes are provided with this report. Both can be compiled with the implementation and header files to show different ways of using the framework of shape classes. The first file provides the use with an friendly interface which enables manual entries of shapes into the array. The contents of the array can then be printed to the screen. The shapes are listed along with their place in the array (Shape No.). This value can be used to delete the shape from the array with the erase function.

\ptab The creation of {\tt Triangle}, {\tt Rectangle} and {\tt Cuboid} shapes is done by providing lengths of their sides, separated by spaces. When the radius and angle is required, and additional letter is necessary to define the angle measure. This can be {\tt r}, {\tt d} or {\tt f} for radians, degrees and fractions of a whole angle respectively. For example, an {\tt fSphere} instantiated with {\tt 1 0.5 f} is a unit hemi-sphere.

\ptab An object of type {\tt Prism} is added by firstly creating a base out of the two dimensional shapes and then adding a third dimension. The base could be, for instance, a segment of a circle which would in turn create a cylindrical segment. Similarly, wedges could be added by adding prisms made with triangular bases.

\ptab The second main code provided with this report shows how the built-in template functionality of certain classes can be used to store integer data. The default type of line segments is {\tt double} but it can be overwritten by explicitly defining the data type. Both rectangles and cuboids can have their sides stored as integers and the calculated values of perimeters, areas and volumes will also be of {\tt int} type. The generic classes are now {\tt Shape$\langle$int$\rangle$} and {\tt Sides$\langle$int$\rangle$}. The code shows an example of creating an array of {\tt vector$\langle$Shape$\langle$int$\rangle$*$\rangle$} type, adding shapes and printing information about them onto the screen.

\ptab The programs were successfully compiled by the GNU Compiler Collection (ver. 4.4.3) with the following commands

\begin{center}
{\tt g++ shapesI.cpp shapesMd.cpp}
\end{center}

\begin{center}
{\tt g++ shapesI.cpp shapesMi.cpp}
\end{center}

for the codes with {\tt double} and {\tt int} types respectively. The header file should be in the same folder as the programs being complied.

\ptab Output 1 presented below has been obtained after adding an {\tt fCircle} object into the array. The input parameters were {\tt 1 90 d} which corresponds to a quarter of a unit circle. As can be seen, the values for perimeter and area correctly evaluate to $(\nicefrac{\pi}{2}+2)$ and $\nicefrac{\pi}{4}$ respectively.

\begin{center}
\begin{tabular}{|l|}
\hline
~\\
{\tt~Shape No : 1}\\
{\tt~~Name~~~~~ : fCircle}\\
{\tt~~Perimeter : 3.5708}\\
{\tt~~Area~~~~~ : 0.785398~~}\\
{\tt~~Volume~~~ : 0}\\
~\\
\hline
\end{tabular}
~\\
~\\
Output 1: One fourth of the unit cycle.
\end{center}
\\

\textbf{4. Discussion and Conclusion}
\\

\ptab Additional functions could be added, for example, in the constructor of the {\tt Triangle} class. These could enable making a triangle from two sides and an angle or one length and two angles. Appropriate routines should use sine and cosine rules to calculate the remaining sides to the triangle.

\ptab The prisms could also be created from the two dimensional shapes which are already in the array. The base could be chosen by its number and information about its perimeter and area would be access without a necessity of creating a new shape.
\\
~\\

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\small{Word count: 1428}

\end{multicols*}
\end{document}
