// Copyright (c) 2010 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

// Bartosz Szczesny - 09 Feb 2010
// PHYS 30762 Programming in C++
// Assignment 2
// Program to compute mean, standard deviation and standard
// error of the mean electronic charge. Data read from file.

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdlib>
#include <cmath>

using namespace std;

int main(void) {
	char filename[100];
	cout << endl << "Enter data filename: ";
	cin >> filename;
	fstream file(filename);
	if (!file.good()) {
		cerr << "Error: File could not be opened." << endl;
		exit(EXIT_FAILURE);
	}

	// read all data points
	int nread = 0;
	const int maxread = 50;
	double* data = new double[maxread];
	for (int i = 0; i < maxread; i++) {
		char dummy[20];
		file >> dummy;
		if (file.fail()) {
			cerr << "Error: Could not read data from file." << endl;
			exit(EXIT_FAILURE);
		}
		double x = strtod(dummy, NULL);
		if (x == 0.0) {
			cout << "Error: Data point " << i + 1 << " is not a number." << endl;
		} else {
			data[nread] = x;
			nread++;
		}
	}
	file.close();
	cout << "  Correct measurements read in: " <<           nread << endl;
	cout << "Incorrect measurements read in: " << maxread - nread << endl;

	double mean  = 0.0;
	double stdev = 0.0;
	double sterr = 0.0;
	for (int i = 0; i < nread; i++) {
		mean += data[i];
	}
	mean = mean / nread;
	for (int i = 0; i < nread; i++) {
		stdev += (data[i] - mean) * (data[i] - mean);
	}
	stdev = sqrt(stdev / (nread - 1));
	sterr = stdev / sqrt(nread);
	cout << setprecision(2) << "Mean charge        = " << mean << "e-19 C" << endl;
	cout << setprecision(2) << "Standard deviation = " << stdev << "e-19 C" << endl;
	cout << setprecision(2) << "Standard error     = " << sterr << "e-19 C" << endl;

	delete[] data;
	cout << "Memory cleared." << endl;
	return 0;
} // int main(void)
