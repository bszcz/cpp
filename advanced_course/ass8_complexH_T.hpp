// Copyright (c) 2010 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

// Bartosz Szczesny /19 Apr 2010/
// PHYS 30762 Programming in C++
// Assigment 8 - Advanced Topics

#ifndef COMPLEX_H_T  // pre-processor directive
#define COMPLEX_H_T  // class defined only once

#include <cmath>
#include <iomanip>
#include <iostream>

using namespace std;

namespace cns { // REMEMBER TO ADD unsing namespace cns;

template <class T> class complex;

template <class T>
ostream& operator<<(ostream &os, const complex<T> &z); // NEED cns:: TO WORK !!!

template <class T>
class complex {

private:
	T re, im;

public:
	// constructors & destructor
	complex()         { re = 0; im = 0; }
	complex(T r, T i) { re = r; im = i; }
	~complex() {}

	complex<T> conj() const; // complex conjugate

	T      real() const; // Re part
	T      imag() const; // Im part
	double mod()  const; // modulus
	double arg()  const; // argument

	complex<T>      operator+(const complex<T> &z) const;  //
	complex<T>      operator-(const complex<T> &z) const;  // OVER
	complex<T>      operator*(const complex<T> &z) const;  // LOAD
	complex<double> operator/(const complex<T> &z) const;  //

	friend ostream& operator<< <T> (ostream &os, const complex<T> &z); // generates output

};  // end of complex class

} // end of cns namespace

#endif // end of pre-processor directive

using namespace cns;

// complex conjugate
template <class T> complex<T> complex<T>::conj() const {
	T c_im = -im;
	if (im == 0) {
		c_im = 0; // avoiding "-0"
	}
	complex<T> c(re, c_im);
	return c;
}

// components
template <class T> T complex<T>::real() const {
	return re;
}

template <class T> T complex<T>::imag() const {
	return im;
}

// polar co-ordinates
template <class T> double complex<T>::mod() const {
	return sqrt(re * re + im * im);
}
template <class T> double complex<T>::arg() const {
	double argument(0);
	if (re == 0 && im == 0) {
		cout << "ERROR: argument of (0 + 0j) undefined" << endl;
	} else {
		argument = atan2(im, re); // atan2(y,x) <- gives correct values for x = 0
	}
	return argument;
}

//
// OVERLOADING
//

// + operator
template <class T>
complex<T> complex<T>::operator+(const complex<T> &z) const {
	complex<T> c(re + z.real(), im + z.imag());
	return c;
}

// - operator
template <class T>
complex<T> complex<T>::operator-(const complex<T> &z) const {
	complex<T> c(re - z.real(), im - z.imag());
	return c;
}

// * operator
template <class T>
complex<T> complex<T>::operator*(const complex<T> &z) const {
	T c_re = re * z.real() - im * z.imag(); // x1*x2 - y1*y2
	T c_im = re * z.imag() + im * z.real(); // x1*y2 + y1*x2
	complex<T> c(c_re, c_im);
	return c;
}

// / operator
template <class T>
complex<double> complex<T>::operator/(const complex<T> &z) const {
	if (z.mod() == 0) {
		cout << "ERROR: division by (0 + 0j)" << endl;
		complex<double> c;
		return c;
	} else {
		double zms = z.mod() * z.mod();
		complex<double> zd(z.real(), z.imag());
		complex<double> w(re, im);
		complex<double> d = w * zd.conj();
		double cre(d.real() / zms), cim(d.imag() / zms);
		complex<double> c(cre, cim);
		return c;
	}
}

// << operator overload (complex class friend)
template <class T>
ostream& cns::operator<<(ostream &os, const complex<T> &z) { // NEED cns:: TO WORK !!!
	if (z.imag() >= 0) {
		os << setprecision(3) << "(" << z.real() << " + " <<  z.imag() << "j)";
	} else {
		os << setprecision(3) << "(" << z.real() << " - " << -z.imag() << "j)";
	}
	return os;
}
