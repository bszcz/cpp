// Copyright (c) 2010 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

// Bartosz Szczesny /24 Mar 2010/
// PHYS 30762 Programming in C++
// Assignment 7 - Polymorphism

#include <cmath>
#include <iostream>

using namespace std;

const double PI = 3.141592654;

// abstract base class for circuit components
class component {
protected:
	double Z; // impedance
	double phi; // phase

public:
	component(double Z_, double phi_) {
		Z = Z_;
		phi = phi_;
	}
	virtual ~component() {};

	// pure virtual functions
	virtual void puts_Z() = 0;
	virtual void puts_phi() = 0;
};

class resistor : public component {
public:
	resistor(double f, double R) : component(R, 0.0) {}

	void puts_Z() {
		cout << "  Z_resistor   = " << Z;
	}
	void puts_phi() {
		cout << "  phi_resistor = " << phi;
	}
};

class capacitor : public component {
public:
	capacitor(double f, double C) : component(1.0 / (2.0 * PI * f * C), -PI / 2.0) {}

	void puts_Z() {
		cout << "  Z_capacitor   = " << Z;
	}
	void puts_phi() {
		cout << "  phi_capacitor = " << phi;
	}
};

class inductor : public component {
public:
	inductor(double f, double L) : component(2.0 * PI * f * L, PI / 2.0) {}

	void puts_Z() {
		cout << "  Z_inductor   = " << Z;
	}
	void puts_phi() {
		cout << "  phi_inductor = " << phi;
	}
};

int main(void) {
	const double mains_freq = 50.0;

	int ncomp;
	cout << "Number of components? : ";
	cin >> ncomp;

	// pointer to an array of base class pointers
	component** comps = new component*[ncomp];

	// ask for component type and its parameter
	for (int i = 0; i < ncomp; i++) {
		char type;
		cout << "Component " << i + 1 << ", enter [r|c|i] for [resistor|capacitor|inductor]: ";
		cin >> type;
		double para;
		cout << " Enter the parameter (resistance, capacitance or inductance): ";
		cin >> para;

		// create new object
		switch (type) {
		case 'r':
			comps[i] = new resistor(mains_freq,  para);
			break;
		case 'c':
			comps[i] = new capacitor(mains_freq, para);
			break;
		case 'i':
			comps[i] = new inductor(mains_freq,  para);
			break;
		default:
			cout << "error: please enter 'r', 'c' or 'i' only" << endl;
			i--;
		}
	}

	for (int i = 0; i < ncomp; i++) {
		cout << "Component " << i + 1 << ":" << endl;
		comps[i]->puts_Z();
		cout << endl;
		comps[i]->puts_phi();
		cout << endl;
	}

	for (int i = 0; i < ncomp; i++) {
		delete comps[i];
		comps[i] = NULL;
	}
	delete[] comps;

	return 0;
}
