// Copyright (c) 2010 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

// Bartosz Szczesny /02 Feb 2010/
// Assignment 1: Program to calculate transition energy using simple Bohr formula

#include <iostream>

using namespace std;

int main(void) {
	// some ASCII art
	cout << "Bohr formula:" << endl;
	cout << "        2 [  1       1  ]   " << endl;
	cout << " dE  = Z  [ ---  -  --- ] Ry" << endl;
	cout << "   ij     [   2       2 ]   " << endl;
	cout << "          [  n       n  ]   " << endl;
	cout << "              j       i     " << endl;

	double Z  = 0.0;
	double ni = 0.0;
	double nj = 0.0;
	bool values_incorrect = true;
	do { // repeat until values correct
		cout << "Please enter the atomic number (Z): ";
		cin >> Z;

		cout << "Please enter the initial principle quantum number (ni): ";
		cin >> ni;

		cout << "Please enter the final   principle quantum number (nj): ";
		cin >> nj;

		if (ni > 0.0 && nj > 0.0 && Z > 0.0) { // check if values correct
			values_incorrect = false;
		} else {
			cout << "Values incorrect, please try again." << endl;
		}
	} while (values_incorrect);

	const double Ry = 2.17987197e-18; // source: http://physics.nist.gov/constants
	double dE = Z * Z * (1 / (nj * nj) - 1 / (ni * ni)) * Ry; // calculate photon energy in Joules
	cout << "Photon energy for this transition is: " << dE << " J" << endl;

	return 0;
} // int main(void)
