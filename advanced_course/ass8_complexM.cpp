// Copyright (c) 2010 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

// Bartosz Szczesny /19 Apr 2010/
// PHYS 30762 Programming in C++
// Assigment 8 - Advanced Topics

#include "ass8_complexH.hpp"

using namespace std;
using namespace cns;

int main(void) {
	// two complex numbers
	complex a(1.1, 1.9);
	complex b(3, -4);

	// real, imaginary and conjugate (using overloading of <<)
	cout << "Complex number a = " << a << ": Re(a) = " << a.real()
	     << ", Im(a) = " << a.imag() << ", a^* = " << a.conj() << endl;
	cout << "Complex number b = " << b << ": Re(b) = " << b.real()
	     << ", Im(b) = " << b.imag() << ", b^* = " << b.conj() << endl << endl;

	// modulus  of a complex number
	cout << "Modulus  of " << a << " is " << a.mod() << endl;
	cout << "Modulus  of " << b << " is " << b.mod() << endl << endl;

	// argument of a complex number
	cout << "Argument of " << a << " is " << a.arg() << endl;
	cout << "Argument of " << b << " is " << b.arg() << endl << endl;

	cout << a << " + " << b << " = " << a + b << endl; // addition    of 2 complex numbers
	cout << a << " - " << b << " = " << a - b << endl; // subtraction of 2 complex numbers
	cout << a << " * " << b << " = " << a * b << endl; // complex multiplication (returns double)
	cout << a << " / " << b << " = " << a / b << endl; // complex division       (returns complex)
	cout << endl;

	return 0;
}
