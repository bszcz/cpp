// Copyright (c) 2010 Bartosz Szczesny <bszcz@bszcz.org>
// This program is free software under the MIT license.

// Bartosz Szczesny /19 Apr 2010/
// PHYS 30762 Programming in C++
// Assigment 8 - Advanced Topics

#include "ass8_complexH.hpp"
#include <cmath>

using namespace std;
using namespace cns;

// complex conjugate
complex complex::conj() const {
	double c_im = -im;
	if (im == 0) {
		c_im = 0; // avoiding "-0"
	}
	complex c(re, c_im);
	return c;
}

// components
double complex::real() const {
	return re;
}

double complex::imag() const {
	return im;
}

// polar co-ordinates
double complex::mod() const {
	return sqrt(re * re + im * im);
}

double complex::arg() const {
	double argument(0);
	if (re == 0 && im == 0) {
		cout << "ERROR: argument of (0 + 0j) undefined" << endl;
	} else {
		argument = atan2(im, re); // atan2(y,x) <- gives correct values for x = 0
	}
	return argument;
}

//
// OVERLOADING
//

// + operator
complex complex::operator+(const complex &z) const {
	complex c(re + z.real(), im + z.imag());
	return c;
}

// - operator
complex complex::operator-(const complex &z) const {
	complex c(re - z.real(), im - z.imag());
	return c;
}

// * operator
complex complex::operator*(const complex &z) const {
	double c_re = re * z.real() - im * z.imag(); // x1*x2 - y1*y2
	double c_im = re * z.imag() + im * z.real(); // x1*y2 + y1*x2
	complex c(c_re, c_im);
	return c;
}

// / operator
complex complex::operator/(const complex &z) const {
	if (z.mod() == 0) {
		cout << "ERROR: division by (0 + 0j)" << endl;
		complex c;
		return c;
	} else {
		double zms = z.mod() * z.mod();
		complex w(re, im);
		complex d = w * z.conj();
		double cre(d.real() / zms);
		double cim(d.imag() / zms);
		complex c(cre, cim);
		return c;
	}
}

// << operator overload (complex class friend)
ostream& cns::operator<<(ostream &os, const complex &z) { // NEED cns:: TO WORK !!!
	if (z.imag() >= 0) {
		os << setprecision(3) << "(" << z.real() << " + " << +z.imag() << "j)";
	} else {
		os << setprecision(3) << "(" << z.real() << " - " << -z.imag() << "j)";
	}
	return os;
}
