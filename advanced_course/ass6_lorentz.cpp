// Copyright (c) 2010 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

// Bartosz Szczesny /15 Mar 2010/
// PHYS 30762 Programming in C++
// Assignment 6 - Inheritance

#include <iostream>
#include <cmath>

using namespace std;

class vec3 {
protected:
	double x_;
	double y_;
	double z_;

public:
	vec3() {
		x_ = 0.0;
		y_ = 0.0;
		z_ = 0.0;
	}
	vec3(double x, double y, double z) {
		x_ = x;
		y_ = y;
		z_ = z;
	}
	~vec3() {}

	double x() const {
		return x_;
	}
	double y() const {
		return y_;
	}
	double z() const {
		return z_;
	}

	// dot product
	double operator*(const vec3& v)  {
		return x_ * v.x() + y_ * v.y() + z_ * v.z();
	}

	friend ostream& operator<<(ostream& os, const vec3& v);
}; // class vec3

ostream& operator<<(ostream& os, const vec3& v) {
	os << "(" << v.x() << ", " << v.y() << ", " << v.z() << ")";
	return os;
}

class vec4 : public vec3 { // derived from vec3 class
private:
	double ct_;

public:
	// constructors, destructor
	vec4() {
		ct_ = 0.0;
	}
	vec4(double ct, double x, double y, double z) : vec3(x, y, z)  {
		ct_ = ct;
	}
	vec4(double ct, vec3& v) : vec3(v.x(), v.y(), v.z()) {
		ct_ = ct;
	}
	~vec4() {}

	double ct() const {
		return ct_;
	}

	// return 3-vector part
	vec3 v3() const {
		vec3 v(x_, y_, z_);
		return v;
	}

	// dot 4-product
	double operator*(const vec4& w)  {
		return ct_ * w.ct() - v3() * w.v3();
	}

	// Lorentz boost in x-direction
	vec4 xboost(double beta) {
		double gamma = 1.0 / sqrt(1.0 - beta * beta);
		vec4 w(gamma * (ct_ - beta * x_), gamma * (x_ - beta * ct_), y_, z_);
		return w;
	}

	friend ostream& operator<<(ostream& os, const vec4& w);
}; // class vec4

ostream& operator<<(ostream& os, const vec4& w) {
	os << "(" << w.ct() << ", " << w.x() << ", " << w.y() << ", " << w.z() << ")";
	return os;
}

int main(void) {
	vec3 v0;
	vec3 v1(0.1, 0.01, 0.001);
	vec3 v2(0.1, 0.4, 0.9);

	cout << "v0 = " << v0 << endl;
	cout << "v1 = " << v1 << endl;
	cout << "v2 = " << v2 << endl;

	// 3-vector product
	cout << "v1.v2 = " << v1 * v2 << endl;

	double beta = 0.3; // beta = speed / speed of light

	vec4 w0;
	vec4 w1(0.11, 0.12, 0.13, 0.14);
	vec4 w2(0.111, v2);

	cout << "w0 = " << w0 << endl;
	cout << "w1 = " << w1 << endl;
	cout << "w2 = " << w2 << endl;

	// change frame
	cout << "w1' = " << w1.xboost(beta) << endl;
	cout << "w2' = " << w2.xboost(beta) << endl;

	// boost invariance
	cout << "w1.w2   = " << w1* w2 << endl;
	cout << "w1'.w2' = " << w1.xboost(beta)*w2.xboost(beta) << endl;

	return 0;
}
