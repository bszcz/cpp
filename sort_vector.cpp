// Copyright (c) 2013 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

#include <algorithm>
#include <iomanip>
#include <iostream>
#include <vector>

using namespace std;

struct {
	const unsigned last_digit = 9;

	bool operator()(unsigned i, unsigned j) {
		if (i <= last_digit && j > last_digit) {
			return true;
		} else {
			return false;
		}
	}
} digits_first;

vector<unsigned> init(const unsigned size) {
	vector<unsigned> v(size);
	for (unsigned i = 0; i < size; i++) {
		v.at(i) = i;
	}
	return v;
}

void print(const vector<unsigned>& v) {
	cout << "vector:";
	for (vector<unsigned>::const_iterator it = v.begin(); it != v.end(); it++) {
		cout << " " << setw(2) << *it;
	}
	cout << endl;
}

int main(void) {
	const unsigned size = 16;
	vector<unsigned> v = init(size);
	print(v);

	random_shuffle(v.begin(), v.end());
	print(v);

	sort(v.begin(), v.end(), digits_first);
	print(v);

	sort(v.begin(), v.end());
	print(v);

	sort(v.begin(), v.end(), greater<unsigned>());
	print(v);

	return 0;
}
