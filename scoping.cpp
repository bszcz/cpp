// Copyright (c) 2013 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

#include <iostream>

using namespace std;

namespace ns {

class C {
public:
	static const string str;

	static void func() {
		cout << "ns::C::func()" << endl;
	}
};

const string str = "ns::str";

void func() {
	cout << "ns::func()" << endl;
}

}; // namespace ns

class C {
public:
	static const string str;

	static void func() {
		cout << "C::func()" << endl;
	}
};

const string str = "::str";

void func() {
	cout << "func()" << endl;
}

// static data members must be initialised out of line
const string C::str = "C::str";
const string ns::C::str = "ns::C::str";

int main(void) {
	const string str = "str";

	cout << str << endl; // local
	cout << ::str << endl; // global
	cout << C::str << endl;
	cout << ns::str << endl;
	cout << ns::C::str << endl;

	cout << endl;

	func();
	::func(); // same as above
	C::func();
	ns::func();
	ns::C::func();

	return 0;
}
