// Copyright (c) 2013 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

// Item 5: Be wary of user-defined conversion functions

#include <iostream>

using namespace std;

class RationalImp {
public:
	int den;
	int num;

	RationalImp(int den_, int num_) {
		den = den_;
		num = num_;
	}

	operator double() const { // implicit conversion
		return double(den) / double(num);
	}
};

class RationalExp {
public:
	int den;
	int num;

	RationalExp(int den_, int num_) {
		den = den_;
		num = num_;
	}

	double asDouble() const { // explicit conversion
		return double(den) / double(num);
	}
};

int main(void) {
	RationalImp r1(1, 2);
	RationalExp r2(1, 2);
	double d = 10.0;

	cout << fixed;
	cout << " r1 * d = " << r1 * d << endl; // implicit double(r1) used here
	cout << " r2 * d = " << r2.asDouble() * d << endl; // explicit asDouble()
	cout << " r1 = " << r1 << endl; // prints "0.5" but "1/2" would be nicer
	//cout << " r2 = " << r2 << endl; // gives error, need to overload "<<"

	return 0;
}
