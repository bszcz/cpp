// Copyright (c) 2013 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

// Item 21: Overload to avoid implicit type conversions

#include <cstdlib>
#include <ctime>
#include <iostream>

using namespace std;

class IntWithRands {
protected:
	int value;
	int* rands;
public:
	IntWithRands(int value_) {
		value = value_;
		srand(value);
		const int nrands = 1e7;
		rands = new int[nrands];
		for (int i = 0; i < nrands; i++) {
			rands[i] = rand() % 10;
		}
	}
	~IntWithRands() {
		delete[] rands;
	}

	friend ostream& operator<<(ostream& os, const IntWithRands& iwr) {
		os << iwr.value << ":{";
		for (int i = 0; i < 10; i++) {
			os << iwr.rands[i] << ",";
		}
		os << "...}";
		return os;
	}

};

class IntWithRandsSlow : public IntWithRands {
public:
	IntWithRandsSlow(int value_) : IntWithRands(value_) {
		cout << "constructing: IntWithRandsSlow(" << value << ")" << endl;
	}

	friend const IntWithRandsSlow operator+(const IntWithRandsSlow& lhs,
			const IntWithRandsSlow& rhs) {
		int tmp = lhs.value + rhs.value;
		return IntWithRandsSlow(tmp);
	}
};

class IntWithRandsFast : public IntWithRands {
public:
	IntWithRandsFast(int value_) : IntWithRands(value_) {
		cout << "constructing: IntWithRandsFast(" << value << ")" << endl;
	}

	friend const IntWithRandsFast operator+(
		const IntWithRandsFast& lhs,
		const IntWithRandsFast& rhs
	) {
		int tmp = lhs.value + rhs.value;
		return IntWithRandsFast(tmp);
	}

	friend const IntWithRandsFast operator+(
		const IntWithRandsFast& lhs,
		const int rhs
	) {
		int tmp = lhs.value + rhs;
		return IntWithRandsFast(tmp);
	}

	friend const IntWithRandsFast operator+(
		const int lhs,
		const IntWithRandsFast& rhs
	) {
		int tmp = lhs + rhs.value;
		return IntWithRandsFast(tmp);
	}
};


int main(void) {
	IntWithRandsSlow iwrs(1);
	IntWithRandsFast iwrf(1);

	cout << iwrs << endl;
	cout << iwrf << endl;

	cout << iwrs + 2 << endl;
	cout << iwrf + 2 << endl;

	cout << "----" << endl;

	clock_t t = clock();
	for (int i = 1; i < 10; i++) {
		iwrs + 10 * i;
	}
	cout << "slow: " << fixed << (double)(clock() - t) / CLOCKS_PER_SEC << "s" << endl;

	cout << "----" << endl;

	t = clock();
	for (int i = 1; i < 10; i++) {
		iwrf + 10 * i;
	}
	cout << "fast: " << fixed << (double)(clock() - t) / CLOCKS_PER_SEC << "s" << endl;

	return 0;
}
