// Copyright (c) 2013 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

// Item 1: Distinguish between pointers and references

#include <iostream>

using namespace std;

class Decimal {
public:
	double value;

	Decimal(double value_) {
		value = value_;
	}
};

void printDecimal(const Decimal& d_ref) {
	// no such thing as null reference
	// and no need to check for it
	cout << "Decimal value: " << d_ref.value << endl;
}

void printDecimal(const Decimal* d_ptr) {
	// need to check for null pointer
	if (d_ptr != NULL) {
		cout << "Decimal value: " << d_ptr->value << endl;
	}
}

int main(void) {
	//Decimal& d_ref; // error: declaration of reference variable requires an initializer
	Decimal* d_ptr = NULL; // this is fine

	Decimal d(0.01);

	Decimal& d_ref = d; // note: no & sign
	d_ptr = &d;
	printDecimal(d_ref); // 0.01
	printDecimal(d_ptr); // 0.01

	Decimal e(2.72);
	d_ref = e; // "d_ref" still refers to "d" which now equals 2.72
	printDecimal(d_ref); // 2.72 - new value of "d"

	d_ptr = &e; // "d_ptr" points to "e"
	d.value = 0.01; // reset "d" to old value
	printDecimal(d_ref); // 0.01 - old value of "d"
	printDecimal(d_ptr); // 2.72 - still points to "e"

	return 0;
}
