// Copyright (c) 2013 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

// Item 6: Distinguish between prefix and postfix forms
//         of increment and decrement operators

#include <iostream>

using namespace std;

class Even {
private:
	int value;

public:
	Even() {
		value = 0;
	}
	Even(int value_) {
		if (value_ % 2 != 0) {
			// if odd, round *towards* zero
			if (value_ > 0) {
				value_--;
			} else {
				value_++;
			}
		}
		value = value_;
	}

	Even& operator++() { // ++prefix
		this->value += 2;
		return *this;
	}
	// use unnamed parameter to avoid
	// warnings about unused parameters
	const Even operator++(int) { // postfix++
		Even old = *this;
		this->value += 2;
		return old;
	}

	friend ostream& operator<<(ostream& os, const Even& e) {
		os << e.value;
		return os;
	}
};

int main(void) {
	Even e(-3);

	cout << e   << endl;
	cout << e++ << endl;
	cout << e   << endl;
	cout << ++e << endl;

	return 0;
}
