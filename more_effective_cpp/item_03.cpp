// Copyright (c) 2013 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

// Item 3: Never treat arrays polymorphically

#include <iostream>

using namespace std;

class Child {
public:
	string name;

	Child() {
		name = "(unnamed)";
	}
};

class Teen : public Child {
public:
	int fwp; // first world problems

	Teen() : Child() {
		fwp = 99;
	}
};

void printNames(Child* array, int len) {
	for (int i = 0; i < len; i++) {
		cout << i << " : " << array[i].name << endl;
		// the pointer arithmetic assumes sizeof(Child)
		// but sizeof(Teen) != sizeof(Child) due to "fwp"
	}
}

int main(void) {
	const int len = 10;

	Child* children = new Child[len];
	Teen* teens = new Teen[len];

	printNames(children, len);
	printNames(teens, len); // runtime error

	return 0;
}
