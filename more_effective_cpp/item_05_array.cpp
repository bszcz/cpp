// Copyright (c) 2013 Bartosz Szczesny
// LICENSE: The MIT License (MIT)

// Item 5: Be wary of user-defined conversion functions

#include <iostream>

using namespace std;

class Array {
public:
	int len;
	int* items;

	Array(int len_) { // need explicit keyword here
		len = len_;
		items = new int[len](); // () initialises to 0
	}
	~Array() {
		delete[] items;
	}

	bool operator==(const Array& arr) const {
		if (len != arr.len) {
			return false;
		}
		for (int i = 0; i < len; i++) {
			if (items[i] != arr.items[i]) {
				return false;
			}
		}
		return true;
	}
};

int main(void) {
	Array a(10);
	Array b(10);

	if (a == b) {
		cout << "pass" << endl;
	} else {
		cout << "fail" << endl;
	}

	b.items[1] = 1;
	if (a == b) {
		cout << "fail" << endl;
	} else {
		cout << "pass" << endl;
	}

	int c = 10;
	if (a == c) { // implicit conversion of 10 into Array(10)
		cout << "fail" << endl;
	} else {
		cout << "pass" << endl;
	}

	return 0;
}
